package com.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.connessione.ConnettoreDB;
import com.model.Categoria;
import com.mysql.jdbc.PreparedStatement;

public class CategoriaDAO implements Dao<Categoria>{

	@Override
	public Categoria getById(int id) throws SQLException {
		
		Categoria temp = new Categoria();
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "SELECT id, nome, descrizione, codice FROM Categoria WHERE id=?";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
		ps.setInt(1, id);
		ps.executeQuery();
		
		ResultSet risultato = ps.getResultSet();
		risultato.next();
		temp.setId(risultato.getInt(1));
		temp.setNome(risultato.getString(2));
		temp.setDescrizione(risultato.getString(3));
		temp.setCodice(risultato.getString(4));
		
		return temp;
	}

	@Override
	public ArrayList<Categoria> getAll() throws SQLException {
		ArrayList<Categoria> elenco = new ArrayList<Categoria>();
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "SELECT id, nome, descrizione, codice FROM Categoria";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
		ps.executeQuery();
		
		ResultSet risultato = ps.getResultSet();
		
		while(risultato.next()) {
			Categoria temp = new Categoria();
			temp.setId(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setDescrizione(risultato.getString(3));
			temp.setCodice(risultato.getString(4));
			elenco.add(temp);
		}
		
		return elenco;
	}

	@Override
	public void insert(Categoria t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "INSERT INTO Categoria (nome, descrizione, codice) VALUE (?, ?, ?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getNome());
		ps.setString(2, t.getDescrizione());
		ps.setString(3, t.getCodice());
		
		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setId(risultato.getInt(1));
	}

	@Override
	public boolean delete(int ID) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "DELETE FROM Categoria WHERE id= ?";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
		ps.setInt(1, ID);
		
		int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	@Override
	public boolean update(Categoria t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "UPDATE Categoria SET nome = ?, descrizione = ? WHERE id= ?";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
		ps.setString(1, t.getNome());
		ps.setString(2, t.getDescrizione());
		ps.setInt(3, t.getId());

		int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

}
