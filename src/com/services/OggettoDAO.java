package com.services;

import java.sql.SQLException;
import java.util.ArrayList;

import com.connessione.ConnettoreDB;
import com.model.Oggetto;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;

public class OggettoDAO implements Dao<Oggetto>{

	@Override
	public Oggetto getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Oggetto> getAll() throws SQLException {
		
		ArrayList<Oggetto> elenco = new ArrayList<Oggetto>();
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "SELECT id, nome, descrizione, codice, prezzo FROM Oggetto";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
		ps.executeQuery();
		
		ResultSet risultato = ps.getResultSet();
		
		while(risultato.next()) {
			Oggetto temp = new Oggetto();
			temp.setId(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setDescrizione(risultato.getString(3));
			temp.setCodice(risultato.getString(4));
			temp.setPrezzo(risultato.getFloat(5));
			elenco.add(temp);
		}
		
		return elenco;
	}

	@Override
	public void insert(Oggetto t) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(Oggetto t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Oggetto t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
