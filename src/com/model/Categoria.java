package com.model;

public class Categoria {

	private int id;
	private String nome;
	private String descrizione;
	private String codice;
	
	public Categoria() {
		
	}
	public Categoria(int varID, String varNome, String varDescrizione, String varCodice) {
		this.setId(varID);
		this.setNome(varNome);
		this.setDescrizione(varDescrizione);
		this.setCodice(varCodice);
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}
	
	
}
