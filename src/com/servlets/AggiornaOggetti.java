package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.model.Oggetto;
import com.services.OggettoDAO;
import com.utility.ResponsoOperazione;

/**
 * Servlet implementation class AggiornaTabella
 */
@WebServlet("/aggiornaoggetti")
public class AggiornaOggetti extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		OggettoDAO oggDao = new OggettoDAO();
		
		try {
			ArrayList<Oggetto> elencoArticoli = oggDao.getAll();
			
			String risultatoJson = new Gson().toJson(elencoArticoli);
			out.print(risultatoJson);
		} catch (SQLException e) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage())));	//Molto compatta
			System.out.println(e.getMessage());
		}
		
		
	}

}
