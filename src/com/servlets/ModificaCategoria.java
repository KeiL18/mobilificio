package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.model.Categoria;
import com.services.CategoriaDAO;
import com.utility.ResponsoOperazione;

@WebServlet("/modificacategoria")
public class ModificaCategoria extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idModifica = Integer.parseInt(request.getParameter("id_categoria"));
		String nomeModifica = request.getParameter("nomeModificare");
		String descrizioneModifica = request.getParameter("descrizioneModificare");
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		CategoriaDAO catDao = new CategoriaDAO();
		
		try {
			Categoria temp = catDao.getById(idModifica);
			temp.setNome(nomeModifica);
			temp.setDescrizione(descrizioneModifica);
			catDao.update(temp);
			
			out.print(new Gson().toJson(new ResponsoOperazione("OK", "")));	
		} catch (SQLException e) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage())));	
			System.out.println(e.getMessage());
		}
		
	}
}
