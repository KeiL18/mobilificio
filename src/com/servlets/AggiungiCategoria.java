package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.model.Categoria;
import com.services.CategoriaDAO;
import com.utility.ResponsoOperazione;

@WebServlet("/aggiungicategoria")
public class AggiungiCategoria extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		CategoriaDAO catDao = new CategoriaDAO();
		
		try {
			Categoria temp = new Categoria();
			temp.setNome(request.getParameter("nome"));
			temp.setDescrizione(request.getParameter("descrizione"));
			temp.setCodice(request.getParameter("codice"));
			catDao.insert(temp);
			
			out.print(new Gson().toJson(new ResponsoOperazione("OK","")));
		} catch (SQLException e) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage())));	//Molto compatta
			System.out.println(e.getMessage());
		}
	}

}
