package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.model.Categoria;
import com.services.CategoriaDAO;
import com.utility.ResponsoOperazione;

@WebServlet("/recuperacategoria")
public class RecuperaCategoria extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int idSelezionato = request.getParameter("id_categoria") != null ? Integer.parseInt(request.getParameter("id_categoria")) : -1;
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		if (idSelezionato != -1) {
			CategoriaDAO catDao = new CategoriaDAO();
			
			try {
				Categoria temp = catDao.getById(idSelezionato);
				
				if(temp != null) {
					System.out.println(new Gson().toJson(temp));
					out.print(new Gson().toJson(temp));
				}
				else {
					out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "Impossibile trovare la categoria nel database")));	
				}
			} catch (SQLException e) {
				out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage())));	
				System.out.println(e.getMessage());
			}
		}
	}
}
