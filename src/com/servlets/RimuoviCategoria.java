package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.services.CategoriaDAO;
import com.utility.ResponsoOperazione;

@WebServlet("/rimuovicategoria")
public class RimuoviCategoria extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int idEliminare = request.getParameter("id_categoria") != null ? Integer.parseInt(request.getParameter("id_categoria")) : -1;

		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		Gson jsonizzatore = new Gson();
		
		if(idEliminare != -1) {
			CategoriaDAO catDao = new CategoriaDAO();
			
			try {
				if(catDao.delete(idEliminare)) {
					ResponsoOperazione res = new ResponsoOperazione("OK", "");
					out.print(jsonizzatore.toJson(res));
				} else {
					ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Nono sono riuscito a fare l'eliminazione");
					out.print(jsonizzatore.toJson(res));
				}
				
			} catch (SQLException e) {
				out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage())));	//Molto compatta
				System.out.println(e.getMessage());
			}
		}
	}
}
