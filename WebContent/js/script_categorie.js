function aggiornaTabella(){
    $.ajax(
        {
            url: "http://localhost:8080/CorsoNTTmobilificio/aggiornacategorie",
            method: "POST",
            success: function(risultato){
                stampaTabella(risultato);
            },
            error: function(errore){
                console.log("ERRORE");
                console.log(errore);
            }
        }
    );
 }

 function stampaTabella(arr_categorie){
    let contenuto = "";

    for(let i=0; i<arr_categorie.length; i++){
		contenuto += lineaTabella(arr_categorie[i]);
	}
	
	$("#contenutoTabella").html(contenuto);
 }

 function lineaTabella(obj_categoria){
	
	let risultato = '<tr data-identificatore="' + obj_categoria.id + '">';
    risultato += '    <td>' + obj_categoria.codice + '</td>';
	risultato += '    <td>' + obj_categoria.nome + '</td>';
	risultato += '    <td>' + obj_categoria.descrizione + '...</td>';
	risultato += '    <td><button type="button" class="btn btn-danger btn-block" onclick="eliminaCategoria(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
	risultato += '    <td><button type="button" class="btn btn-warning btn-block" onclick="modificaCategoria(this)"><i class="fa fa-pencil" aria-hidden="true"></i></button></td>';
	risultato += '</tr>';
	
	return risultato;
}

function eliminaCategoria(objButton){
	let idSelezionato = $(objButton).parent().parent().data("identificatore");
	
	$.ajax(
			{
				url: "http://localhost:8080/CorsoNTTmobilificio/rimuovicategoria",
				method: "POST",
				data: {
					id_categoria: idSelezionato
				},
				success: function(responso){			//Senza JSON Parse perché la response è in application/json
					switch(responso.risultato){
					case "OK":
						aggiornaTabella();
						alert("Eliminato con successo");
						break;
					case "ERRORE":
						alert("ERRORE DI ELIMINAZIONE :(\n" + responso.dettaglio);
						break;
					}
					
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
}

function modificaCategoria(objButton){
	let idSelezionato = $(objButton).parent().parent().data("identificatore");
	
	$.ajax(
			{
				url: "http://localhost:8080/CorsoNTTmobilificio/recuperacategoria",
				method: "POST",
				data: {
					id_categoria: idSelezionato
				},
				success: function(responso){
					$("#inputNomeMod").val(responso.nome);
					$("#inputDescrizioneMod").val(responso.descrizione);
					$("#modaleModificaCategoria").data("identificatore", idSelezionato);

					$("#modaleModificaCategoria").modal("show");
					
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
}

function eseguiModificaCategoria(newNome, newDescrizione, idModificare){
	
	$.ajax(
			{
				url: "http://localhost:8080/CorsoNTTmobilificio/modificacategoria",
				method: "POST",
				data: {
					id_categoria: idModificare,
					nomeModificare : newNome,
					descrizioneModificare : newDescrizione
				},
				success: function(responso){
					switch(responso.risultato){
					case "OK":
						aggiornaTabella();
						alert("Modificato con successo");
						break;
					case "ERRORE":
						alert("ERRORE DI Modifica :(\n" + responso.dettaglio);
						break;
					}
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
}

function aggiungiCategoria(newNome, newDescrizione, newCodice){
	$.ajax(
			{
				url: "http://localhost:8080/CorsoNTTmobilificio/aggiungicategoria",
				method: "POST",
				data: {
					nome : newNome,
					descrizione : newDescrizione,
					codice : newCodice
				},
				success: function(responso){
					switch(responso.risultato){
					case "OK":
						aggiornaTabella();
						alert("Aggiunto con successo");
						break;
					case "ERRORE":
						alert("ERRORE DI AGGIUNTA :(\n" + responso.dettaglio);
						break;
					}
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
}

 $(document).ready(
    function(){
        aggiornaTabella();

        $("#aggiornaTab").click(
			function(){
				aggiornaTabella();
			}
        );

		$("#aggiungiCat").click(
			function(){
				$("#modaleAggiuntaCategoria").modal("show");
				
				aggiornaTabella();
			}
		);

		$("#cta-modificaCategoria").click(
			function(){
				let newNome = $("#inputNomeMod").val();
				let newDescrizione = $("#inputDescrizioneMod").val();
				let idModificare = $("#modaleModificaCategoria").data("identificatore");
				
				eseguiModificaCategoria(newNome, newDescrizione, idModificare);
				
				$("#modaleModificaCategoria").modal("toggle");
			}
		);
		
		$("#cta-AggiuntaCategoria").click(
			function(){
				let newNome = $("#inputNome").val();
				let newDescrizione = $("#inputDescrizione").val();
				let newCodice = $("#inputCodice").val();
				
				aggiungiCategoria(newNome, newDescrizione, newCodice);
				
				$("#modaleAggiuntaCategoria").modal("toggle");
			}
		);
    }
 );