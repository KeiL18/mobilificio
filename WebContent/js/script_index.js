/**
 * 
 */

function aggiornaTabella(){
    $.ajax(
        {
            url: "http://localhost:8080/CorsoNTTmobilificio/aggiornaoggetti",
            method: "POST",
            success: function(risultato){
                stampaTabella(risultato);
            },
            error: function(errore){
                console.log("ERRORE");
                console.log(ris_error);
            }
        }
    );
 }

 function stampaTabella(arr_articoli){
    let contenuto = "";

    for(let i=0; i<arr_articoli.length; i++){
		contenuto += lineaTabella(arr_articoli[i]);
	}
	
	$("#contenutoTabella").html(contenuto);
 }

 function lineaTabella(obj_articolo){
	
	let risultato = '<tr data-identificatore="' + obj_articolo.id + '">';
    risultato += '    <td>' + obj_articolo.codice + '</td>';
	risultato += '    <td>' + obj_articolo.nome + '</td>';
	risultato += '    <td>' + obj_articolo.prezzo + '</td>';
	risultato += '    <td>' + obj_articolo.descrizione.substring(0, 30) + '...</td>';
	risultato += '    <td><button type="button" class="btn btn-danger btn-block" onclick="eliminaArticolo(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
	risultato += '    <td><button type="button" class="btn btn-warning btn-block" onclick="modificaArticolo(this)"><i class="fa fa-pencil" aria-hidden="true"></i></button></td>';
	risultato += '</tr>';
	
	return risultato;
}


 $(document).ready(
    function(){
        aggiornaTabella();

        $("#aggiornaTab").click(
			function(){
				console.log("Sto aggiornando");
				aggiornaTabella();
			}
        );

        $("#aggiungiOgg").click(
            //TODO
        );
    }
 );