DROP DATABASE IF EXISTS lez_29_mobilificio;
CREATE DATABASE lez_29_mobilificio;
USE lez_29_mobilificio;

CREATE TABLE Oggetto(
	id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nome VARCHAR(150) NOT NULL,
    descrizione VARCHAR(150),
    codice VARCHAR(15) NOT NULL UNIQUE,
    prezzo FLOAT NOT NULL
);
INSERT INTO Oggetto(nome, descrizione, codice, prezzo) VALUES
	("Armadio 2 ante - NERO", "", "ARM552", 180.99),
    ("Armadio 4 ante - NERO", "", "ARM554", 259.99),
    ("Sedia da ufficio MARKUS", "", "SED123", 109.99),
    ("Scrivania elegante in massello", "", "SCR123", 500);

CREATE TABLE Categoria(
	id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nome VARCHAR(150) NOT NULL,
    descrizione VARCHAR(150),
    codice VARCHAR(15) NOT NULL UNIQUE
);
INSERT INTO Categoria(nome, codice) VALUES
	("Camera da letto", "CAMADU"),
    ("Ufficio", "CAMUFF"),
    ("Cabina armadio", "CABARM"),
    ("Cucina", "CUCU");
    
CREATE TABLE Oggetto_Categoria(
	oggettoID INTEGER NOT NULL,
    categoriaID INTEGER NOT NULL,
    PRIMARY KEY (oggettoID, categoriaID),
    FOREIGN KEY (oggettoID) REFERENCES Oggetto(id) ON DELETE CASCADE,
    FOREIGN KEY (categoriaID) REFERENCES Categoria(id) ON DELETE CASCADE
);
INSERT INTO Oggetto_Categoria (oggettoID, categoriaID) VALUES
	(1,1), (1,3),
    (2,1), (2,3),
    (3,2),
    (4,1), (4,2);


SELECT * FROM Oggetto;
SELECT * FROM Categoria;

SELECT id, nome, descrizione, codice, prezzo FROM Oggetto;

INSERT INTO Oggetto(nome, descrizione, codice, prezzo) VALUES
	("Lampada da parete", "Led 12W", "LAM12W", 24.99);